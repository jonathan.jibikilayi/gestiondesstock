package tn.esprit.gestiondestock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionDestockApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionDestockApplication.class, args);
	}

}
