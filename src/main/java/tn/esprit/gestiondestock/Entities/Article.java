package tn.esprit.gestiondestock.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ManyToAny;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="Article")
public class Article  extends AbstractEntity{
	
	@Column(name="codearticle")
	
	private String code;
	
	@Column(name="designation")
	private String designation;
	
	@Column(name="prixunitaireht")
	private float prixunitaireHt;

	@Column(name="tauxtva")
	private float tauxTva;
	
	@Column(name="prixunitaire")
	private float prixUnitaire;
	
	@Column(name="photo")
	private String photo;
	
	@ManyToOne
	@JoinColumn(name = "idcategory")
	private Categories category;
}
