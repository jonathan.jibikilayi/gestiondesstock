package tn.esprit.gestiondestock.Entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="MvtStock")
public class MvtStock extends AbstractEntity {
	@Column(name="dateMvt")
     private Date dateMvt;
	
	@Column(name="quantité")
	private float quantité;
	
	@Column(name="typemvt")
	private TypeMvtStock typeMvt;
	
	
	@ManyToOne
	@JoinColumn(name="idarticle")
	private Article article;
	
	

}
