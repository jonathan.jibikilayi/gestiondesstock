package tn.esprit.gestiondestock.Entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="Utilisateur")
public class Utilisateur extends AbstractEntity {
	
	@Column(name ="nom")
	private String nom;
	
	@Column(name ="prenom")
	private String prenom;
	
	@Column(name ="datenaissance")
	private Date datenaissance;
    
	@Embedded
	private Adresse adresse;
	
	@Column(name ="photo")
    private String photo;
	
	@Column(name ="numtel")
    private String numTel;
	
	@Column(name ="mail")
	private String mail;

	
	@Column(name ="motdepasse")
	private String motDePasse;
	
	@ManyToOne
	@JoinColumn(name="identreprise")
	private Entreprise entreprise;
	
	@OneToMany(mappedBy = "utilisateur")
	private List<Role>roles;
	
	
}
