package tn.esprit.gestiondestock.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="LigneVente")
public class LigneVente extends AbstractEntity {
	@ManyToOne
	@JoinColumn(name="idvente")
	private Vente vente;
	 @Column(name="quatite")
	    private float quantité;
	    
	    @Column(name="prixunitaire")
	    private float prixUnitaire;
}
