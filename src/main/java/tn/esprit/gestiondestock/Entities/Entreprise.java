package tn.esprit.gestiondestock.Entities;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="Entreprise")
public class Entreprise extends AbstractEntity {
	
	@Column(name="nom")
	private String  nom;
	
	@Embedded
	private Adresse adresse;
	
	@Column(name ="photo")
    private String photo;
	
	@Column(name ="numtel")
    private String numTel;
	
	@Column(name ="mail")
	private String mail;
	
	@Column(name ="siteweb")
	private String siteWeb;
	
	@Column(name ="description")
	private String description;

}
